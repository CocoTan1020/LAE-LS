allennlp==0.8.4
bert_score==0.3.13
click==7.1.2
nltk==3.6.1
numpy==1.21.5
openai==0.27.8
overrides==7.4.0
pandas==1.4.3
plotly==5.9.0
python_Levenshtein==0.23.0
sacrebleu==2.3.1
sacremoses==0.0.43
scikit_learn==1.3.2
scipy==1.6.2
simalign==0.4
stanfordnlp==0.2.0
syllapy==0.7.2
torch==1.12.1
torchfile==0.1.0
tqdm==4.59.0
transformers==4.14.1
tupa==1.4.2
ucca==1.3.11
yattag==1.15.2
